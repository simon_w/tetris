package code
package comet

import net.liftweb._
import http._
import actor._
import de.htwg.mps.tetris._
import code.snippet.ControlGameMenueSnippet

/**
 * A singleton that provides chat features to all clients.
 * It's an Actor so it's thread-safe because only one
 * message will be processed at once.
 */
object TetrisServer extends LiftActor with ListenerManager {
  private var status = "Welcome to Tetris" // private state

  /**
   * When we update the listeners, what message do we send?
   * We send the msgs, which is an immutable data structure,
   * so it can be shared with lots of threads without any
   * danger or locking.
   */
  def createUpdate = status

  /**
   * process messages that are sent to the Actor.  In
   * this case, we're looking for Strings that are sent
   * to the ChatServer.  We append them to our Vector of
   * messages, and then update all the listeners.
   */
  override def lowPriority = {
    case s: String => {
      s match {
        case "Start" => { Tetris.controller.startGame; notifyViews }
        case "Pause" => { Tetris.controller.pauseGame; notifyViews }
        case "New Game" => { Tetris.controller.newGame(); notifyViews }
        case "^" => { Tetris.controller.model.models(0).updateBlock(0, 0, 1); notifyViews }
        case "v" => { Tetris.controller.model.models(0).updateBlock(0, 1, 0); notifyViews }
        case "<" => { Tetris.controller.model.models(0).updateBlock(-1, 0, 0); notifyViews }
        case ">" => { Tetris.controller.model.models(0).updateBlock(1, 0, 0); notifyViews }
        case "Drop" => {Tetris.controller.model.models(0).moveInstantlyDownAndPersistBlock; notifyViews}
      }
      //      Sudoku.tui.processInputLine(s)
      //      status = Sudoku.controller.statusText
      //      updateListeners()
    }
  }
  
  def notifyViews = Tetris.controller.notifyViews
}
