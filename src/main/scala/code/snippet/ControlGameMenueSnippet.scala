package code.snippet
import scala.xml.Group
import scala.xml.NodeSeq

import code.comet.TetrisServer
import de.htwg.mps.tetris.Tetris
import net.liftweb.http.js.JsCmd.unitToJsCmd
import net.liftweb.http.js.JsCmd
import net.liftweb.http.SHtml
import net.liftweb.util.Helpers.bind
import net.liftweb.util.Helpers.strToSuperArrowAssoc

class ControlGameMenueSnippet {

  val ajaxBtnGameStartPauseName = if (!Tetris.controller.started) "Start" else "Pause"
  val ajaxBtnNewGameName = "New Game"
  val ajaxBtnUpName = "^"
  val ajaxBtnDownName = "v"
  val ajaxBtnLeftName = "<"
  val ajaxBtnRightName = ">"
  val ajaxBtnDropName = "Drop"

  def ajaxFuncGameStartPause: JsCmd = {
    TetrisServer ! ajaxBtnGameStartPauseName
  }

  def ajaxFuncNewGame: JsCmd = {
    TetrisServer ! ajaxBtnNewGameName
  }

  def ajaxFuncBtnUp: JsCmd = {
    TetrisServer ! ajaxBtnUpName
  }

  def ajaxFuncBtnDown: JsCmd = {
    TetrisServer ! ajaxBtnDownName
  }

  def ajaxFuncBtnLeft: JsCmd = {
    TetrisServer ! ajaxBtnLeftName
  }

  def ajaxFuncBtnRight: JsCmd = {
    TetrisServer ! ajaxBtnRightName
  }
  
  def ajaxFuncBtnDrop: JsCmd = {
    TetrisServer ! ajaxBtnDropName
  }

  def renderAjaxControls(html: Group): NodeSeq = {
    bind("controlGame", html,
      "buttonGameStart" -> SHtml.ajaxButton(ajaxBtnGameStartPauseName, ajaxFuncGameStartPause _),
      "buttonNewGame" -> SHtml.ajaxButton(ajaxBtnNewGameName, ajaxFuncNewGame _),
      "buttonUp" -> SHtml.ajaxButton(ajaxBtnUpName, ajaxFuncBtnUp _),
      "buttonDown" -> SHtml.ajaxButton(ajaxBtnDownName, ajaxFuncBtnDown _),
      "buttonLeft" -> SHtml.ajaxButton(ajaxBtnLeftName, ajaxFuncBtnLeft _),
      "buttonRight" -> SHtml.ajaxButton(ajaxBtnRightName, ajaxFuncBtnRight _),
      "buttonDrop" -> SHtml.ajaxButton(ajaxBtnDropName, ajaxFuncBtnDrop _))
  }

}