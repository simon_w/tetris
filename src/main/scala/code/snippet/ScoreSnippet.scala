package code.snippet
import net.liftweb.http.S
import de.htwg.mps.tetris.Tetris

class ScoreSnippet {

  def render = {
    val indexString = S.attr("singleGameModelIndex") openOr "singleGameModelIndex is not defined"
    val singleGameModel = Tetris.controller.model.models(indexString.toInt)

    <div id="score">{
      <div>{
        "level: " + singleGameModel.scorer.level
      }</div>
      <div>{
        "score: " + singleGameModel.scorer.score
      }</div>
      <div>{
        "lines: " + singleGameModel.scorer.rows
      }</div>
    }</div>
  }

}