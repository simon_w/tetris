package code.snippet
import scala.xml.Group
import scala.xml.NodeSeq
import de.htwg.mps.tetris.controller.GuiPlayer
import de.htwg.mps.tetris.Tetris
import net.liftweb.common.Full
import net.liftweb.http.js.JsCmd.unitToJsCmd
import net.liftweb.http.js.JsCmd
import net.liftweb.http.SHtml
import net.liftweb.util.Helpers.bind
import net.liftweb.util.Helpers.strToSuperArrowAssoc
import de.htwg.mps.tetris.controller.aiplayers.StupidAIPlayer

class GameSettingsSnippet {

  var numberOfPlayers = Tetris.controller.model.noOfPlayers
  val kindOfPlayers = List("GUI-Player left", "GUI-Player right", "StupidAI-Player")
  //val kindOfPlayers: Map[String, Int] = Map("GUI-Player left" -> 0, "GUI-Player right" -> 1, "StupidAI-Player" -> 2)

  def renderSelectNumberOfPlayers(html: Group): NodeSeq = {
    //bind view to functionality
    bind("gameSettings", html,
      "selectNumberOfPlayers" -> doSelectNumberOfPlayers _)
  }

  def renderConfigureKindOfPlayers = {
    //    for (i <- 0 until numberOfPlayers) yield {
    //      <div>{
    //        "Player " + i + 1 + ":"
    //        <div>{
    //          <lift:GameSettingsSnippet.renderSelectKindOfPlayers>
    //            <gameSettings:selectKindOfPlayers/>
    //          </lift:GameSettingsSnippet.renderSelectKindOfPlayers>
    //        }</div>
    //      }</div>
    //    }

    /*
    if (numberOfPlayers == 3) {
      <div>{
        <lift:GameSettingsSnippet.renderSelectKindOfPlayers>
          <gameSettings:selectKindOfPlayers0/>
          <gameSettings:selectKindOfPlayers1/>
          <gameSettings:selectKindOfPlayers2/>
        </lift:GameSettingsSnippet.renderSelectKindOfPlayers>
      }</div>
    } else if (numberOfPlayers == 2) {
      <div>{
        <lift:GameSettingsSnippet.renderSelectKindOfPlayers>
          <gameSettings:selectKindOfPlayers0/>
          <gameSettings:selectKindOfPlayers1/>
        </lift:GameSettingsSnippet.renderSelectKindOfPlayers>
      }</div>
    } else if (numberOfPlayers == 1) {
      <div>{
        <lift:GameSettingsSnippet.renderSelectKindOfPlayers>
          <gameSettings:selectKindOfPlayers0/>
        </lift:GameSettingsSnippet.renderSelectKindOfPlayers>
      }</div>
    }
    */
  }

  def renderSelectKindOfPlayers(html: Group) = {
    //bind view to functionality
    /*
    if (numberOfPlayers == 3) {
      bind("gameSettings", html,
        "selectKindOfPlayers0" -> doSelectKindOfPlayers0 _,
        "selectKindOfPlayers1" -> doSelectKindOfPlayers1 _,
        "selectKindOfPlayers2" -> doSelectKindOfPlayers2 _)
    } else if (numberOfPlayers == 2) {
      bind("gameSettings", html,
        "selectKindOfPlayers0" -> doSelectKindOfPlayers0 _,
        "selectKindOfPlayers1" -> doSelectKindOfPlayers1 _)
    } else if (numberOfPlayers == 1) {
      bind("gameSettings", html,
        "selectKindOfPlayers0" -> doSelectKindOfPlayers0 _)
    }
    */
  }

  def doSelectNumberOfPlayers(msg: NodeSeq) = {
    SHtml.ajaxSelect((1 to 3).toList.map(i => (i.toString(), i.toString())), Full(Tetris.controller.model.noOfPlayers.toString()), setNumberOfPlayers(_))
  }

  def setNumberOfPlayers(selected: String) {
    numberOfPlayers = selected.toInt
    Tetris.controller.setNumberOfPlayers(numberOfPlayers)
  }

  //  def doSelectKindOfPlayers0(msg: NodeSeq) = { doSelectKindOfPlayers(0, msg) }
  //  def doSelectKindOfPlayers1(msg: NodeSeq) = { doSelectKindOfPlayers(1, msg) }
  //  def doSelectKindOfPlayers2(msg: NodeSeq) = { doSelectKindOfPlayers(2, msg) }

  //  def doSelectKindOfPlayers(playerIndex: Int, msg: NodeSeq) = {
  //    SHtml.ajaxSelect(kindOfPlayers.map(i => (i.toString(), i.toString())), Full(Tetris.controller.model.nbOfPlayer.toString()), setKindOfPlayer(playerIndex, _))
  //  }

  //  def setKindOfPlayer(playerIndex: Int, selected: String) {
  //    selected match {
  //      case "GUI-Player left" => { Tetris.controller.setPlayer(playerIndex, 0) }
  //      case "GUI-Player right" => { Tetris.controller.setPlayer(playerIndex, 1) }
  //      case "StupidAI-Player" => { Tetris.controller.setPlayer(playerIndex, 2) }
  //    }
  //  }

}