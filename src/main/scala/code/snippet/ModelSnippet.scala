package code.snippet
import de.htwg.mps.tetris.Tetris
import org.joda.time.DateTime

class ModelSnippet {
  
  def render = {
    <div id="tetrisModel">
      {
        for (singleGameModel <- Tetris.controller.model.models) yield {
          <div id="singleGameModel">
            {
              <div class={ "lift:ScoreSnippet?singleGameModelIndex=" + Tetris.controller.model.models.indexOf(singleGameModel) }/>
              <div class={ "lift:NextBlockSnippet?singleGameModelIndex=" + Tetris.controller.model.models.indexOf(singleGameModel) }/>
              <div class={ "lift:TetrisGridSnippet?singleGameModelIndex=" + Tetris.controller.model.models.indexOf(singleGameModel) }/>
            }
          </div>
        }
      }
    </div>
  }
}