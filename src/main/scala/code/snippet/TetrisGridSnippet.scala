package code.snippet
import net.liftweb.http.S
import de.htwg.mps.tetris.Tetris

class TetrisGridSnippet {

  def render = {
    val indexString = S.attr("singleGameModelIndex") openOr "singleGameModelIndex is not defined"
    val singleGameModel = Tetris.controller.model.models(indexString.toInt)

    <div id="tetrisGrid">
      {
        <table>
          {
            <tbody>
              {
                for (y <- 0 until singleGameModel.getGrid.height) yield {
                  <tr>
                    {
                      for (x <- 0 until singleGameModel.getGrid.width) yield {
                        if (y >= singleGameModel.currentBlock.posY && y <= singleGameModel.currentBlock.posY + singleGameModel.currentBlock.shape.length - 1 &&
                          x >= singleGameModel.currentBlock.posX && x <= singleGameModel.currentBlock.posX + singleGameModel.currentBlock.shape.length - 1 &&
                          singleGameModel.currentBlock.shape(singleGameModel.currentBlock.itsRotation)(y - singleGameModel.currentBlock.posY)(x - singleGameModel.currentBlock.posX) == 1) {
                          <td class={
                            "squareType" + singleGameModel.currentBlock.squareType
                          }/>
                        } else {
                          <td class={
                            "squareType" + singleGameModel.getGrid.squares(y)(x).squareType
                          }/>
                        }
                      }
                    }
                  </tr>
                }
              }
            </tbody>
          }
        </table>
      }
    </div>
  }

}