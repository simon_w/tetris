package code.snippet
import net.liftweb.http.S
import de.htwg.mps.tetris.Tetris

class NextBlockSnippet {

  def render = {
    val indexString = S.attr("singleGameModelIndex") openOr "singleGameModelIndex is not defined"
    val singleGameModel = Tetris.controller.model.models(indexString.toInt)

    <div id="nextBlock">
      {
        <table>{
          <tbody>{
            for (y <- 0 until singleGameModel.nextBlock.shape.length) yield {
              <tr>{
                for (x <- 0 until singleGameModel.nextBlock.shape.length) yield {
                  if (singleGameModel.nextBlock.shape(singleGameModel.nextBlock.itsRotation)(y)(x) != 0) {
                    <td class={ "squareType" + singleGameModel.nextBlock.squareType }/>
                  } else {
                    <td class={ "squareType" + 0 }/>
                  }
                }
              }</tr>
            }
          }</tbody>
        }</table>
      }
    </div>
  }

}