package de.htwg.mps.tetris

import de.htwg.mps.tetris.controller.aiplayers.StupidAIPlayer
import de.htwg.mps.tetris.controller.Controller
import de.htwg.mps.tetris.controller.GuiPlayer
import de.htwg.mps.tetris.model.Grid
import de.htwg.mps.tetris.model.Model
import de.htwg.mps.tetris.view.SwingView
import de.htwg.mps.tetris.view.TextView
import de.htwg.mps.tetris.controller.ControllerPublisher
import de.htwg.mps.tetris.controller.ControllerPublisher
import de.htwg.mps.tetris.controller.TetrisModelChanged
import java.awt.Color
import de.htwg.mps.tetris.controller.Player
import de.htwg.mps.tetris.controller.aiplayers.MasochisticAIPlayer
import de.htwg.mps.tetris.controller.aiplayers.SmartAIPlayer
import de.htwg.mps.tetris.controller.ConsolePlayer

object Tetris {

  val model = new Model(noOfPlayers = 1, gridWidth = 10, gridHeight = 20)
  val controller = new Controller(model)

  "GUI" match {
    case "TUI" =>
      val tui = new TextView(controller)
      controller.addView(tui)
      new ConsolePlayer(controller, 0)
      
    case "GUI" =>
      val tui = new TextView(controller)
      val gui = new SwingView(controller)

      controller.addView(tui)
      controller.addView(gui)

      new GuiPlayer(gui.panel, controller, 0, Player.LeftKeys)
      new ConsolePlayer(controller, 0)
  }

  // For demo only
  //model.models(0).grid.generateTestCells

  def main(args: Array[String]) {
    controller.startGame
  }
}
