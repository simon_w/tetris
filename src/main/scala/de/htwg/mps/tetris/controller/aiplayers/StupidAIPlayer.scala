package de.htwg.mps.tetris.controller.aiplayers

import de.htwg.mps.tetris.controller.Player
import de.htwg.mps.tetris.controller.Controller
import scala.util.Random
import scala.actors.Actor._

/**
 * Some AI Player playing tetris in the very beginning of evolution
 */
class StupidAIPlayer(controller: Controller, playerId: Int) extends Player(controller, playerId) {

  val playerActor = actor {
	  while(!detached) {
	    Random.nextInt(7) match {
	      case 1 => moveLeft
	      case 2 => moveRight
	      case 3 => rotateRight
	      case 4 => rotateLeft
	      case 5 => moveDown
	      case 6 => if(Random.nextInt(10) <= 1) moveInstantlyDown
	      case _ => 
	    }
	    
	    Thread.sleep(100 + Random.nextInt(500))
	  }
  }
}