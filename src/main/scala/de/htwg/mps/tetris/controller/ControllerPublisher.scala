package de.htwg.mps.tetris.controller
import scala.swing.Publisher
import scala.swing.event.Event

case class TetrisModelChanged extends Event 

class ControllerPublisher extends Publisher{
  def publishEvent(e:scala.swing.event.Event){
    publish(e)
  }
}