package de.htwg.mps.tetris.controller

import scala.swing.event.KeyPressed
import scala.swing.event.Key
import scala.swing.event.KeyReleased
import scala.actors.Actor._
import scala.actors.Actor

class AutorepeatKeyListener(keyboardBinding: Key.Value => () => Unit) {

  val autorepeatDelay = 200; //ms to start autorepeat
  val autorepeatFreq = 50; //ms between two autorepeats

  def keyPressed(e: KeyPressed) = keyStateChanged(e.key, true)
  def keyReleased(e: KeyReleased) = keyStateChanged(e.key, false)

  def keyStateChanged(keyCode: Key.Value, pressed: Boolean) {

    def functionToCall = keyboardBinding(keyCode)

    if (functionToCall != null) {
      if (pressed) {
        autorepeatActor ! functionToCall
      } else {
        autorepeatActor ! Stop
      }
    }
  }

  case object Stop
  case class Repeat(id: Long)
  case class Sleep(time: Int, id: Long)

  var autorepeatActor: Actor = actor {

    var function: () => Unit = null
    var currentId: Long = 0

    /**
     * helper actor for sleeps
     * When it receives sleeps and tells autorepeatActor
     */
    var helperActor: Actor = actor {
      while (true) {
        receive {
          case Sleep(time, id) => {
            Thread.sleep(time)
            autorepeatActor ! Repeat(id)
          }
        }
      }
    }

    // main actor loop, owns function and currentId vars
    loop {
      react {
        case fn: (() => Unit) => if (function == null) {
          function = fn
          currentId = System.currentTimeMillis
          function()
          helperActor ! Sleep(autorepeatDelay, currentId)
        }
        case Repeat(id) => if (function != null && id == currentId) {
          function()
          helperActor ! Sleep(autorepeatFreq, id)
        }
        case Stop => function = null;
      }
    }
  }
}