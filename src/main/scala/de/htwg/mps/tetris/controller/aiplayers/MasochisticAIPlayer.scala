package de.htwg.mps.tetris.controller.aiplayers

import de.htwg.mps.tetris.controller.Player
import de.htwg.mps.tetris.controller.Controller
import scala.util.Random
import scala.actors.Actor._
import de.htwg.mps.tetris.model.Model
import de.htwg.mps.tetris.model.SingleGameModel
import de.htwg.mps.tetris.model.Block

/**
 * Hates Tetris and hates winning Tetris games -> wants to loose as soon as possible
 * How? Rotates all pieces upright, then hard drops
 */
class MasochisticAIPlayer(controller: Controller, playerId: Int) extends Player(controller, playerId) {

  val playerActor = actor {

    var lastBlock: Block = null

    while (!detached) {
      if (currentBlock != lastBlock) {
        currentBlock.move(rotation = 1)
        lastBlock = currentBlock
      }
      else {
        moveInstantlyDown
      }

      Thread.sleep(500)
    }
  }

  def ownModel: SingleGameModel = controller.model.models(playerId)
  def currentBlock: Block = ownModel.currentBlock
}