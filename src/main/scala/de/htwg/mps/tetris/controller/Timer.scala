package de.htwg.mps.tetris.controller
import scala.actors.Actor
import scala.actors.TIMEOUT

class Timer(interval: Int) extends Actor {
  var timerListeners: List[Actor] = List()

  private val resolution = 20
  private var timeout = interval
  private var time = 0

  def addListener(newListener: Actor) = timerListeners = newListener :: timerListeners

  def setTimeout(newInterval: Int) = {
    if (newInterval != timeout) {
      timeout = newInterval
    }
  }

  case object Stop
  case object Timeout

  def act = {
    loop {
      receiveWithin(resolution) {
        case TIMEOUT => {
          if (time >= timeout) {
            timerListeners.foreach(listener => { listener ! Timeout })
            time = 0
          } else {
            time += resolution
          }
        }
        case Stop => { this.exit(); println("Exit") }
      }
    }
  }

}