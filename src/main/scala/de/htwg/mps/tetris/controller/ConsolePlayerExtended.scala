package de.htwg.mps.tetris.controller
import java.awt.KeyEventDispatcher
import java.awt.DefaultKeyboardFocusManager
import java.awt.event.KeyEvent
import scala.swing.MainFrame
import scala.swing.event.KeyPressed
import scala.swing.event.KeyReleased
import java.awt.event.KeyListener
import java.awt.event.KeyEvent
import scala.swing.BorderPanel

class ConsolePlayerExtended(controller: Controller, playerId: Int) extends Player(controller, playerId) {

  val keyboardInput = new AutorepeatKeyListener(getKeyboardBinding(Player.RightKeys))
  val dummyFrame = new MainFrame {
    contents = new BorderPanel {
      listenTo(this.keys)
      reactions += {
        case e: KeyPressed => println(e.key); keyboardInput.keyPressed(e)
        case e: KeyReleased => println(e.key); keyboardInput.keyReleased(e)
      }
      focusable = true
      requestFocus
    }
  }
  
  override def detach = {
    dummyFrame.contents(0).deafTo(dummyFrame.contents(0).keys)
  }


  dummyFrame.open

}