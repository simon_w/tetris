package de.htwg.mps.tetris.controller

import scala.swing.event.Key

object Player {
  class Keys
  case object LeftKeys extends Keys
  case object RightKeys extends Keys
}

abstract class Player(controller: Controller, playerId: Int) {
   
  // ignore ConsolePlayer (can be attached in parallel to other players)
  this match {
    case _:ConsolePlayer =>
    case _ => controller.registerPlayer(playerId, this)
  }

  def moveLeft = move(-1)
  def moveRight = move(1)
  def rotateLeft = rotate(-1)
  def rotateRight = rotate(1)
  def moveDown = controller.moveDown(playerId)
  def moveInstantlyDown = controller.moveInstantlyDown(playerId)
  def pause = controller.togglePause
  
  var detached = false
  def detach = detached = true

  private def move(direction: Int) = controller.move(playerId, direction)
  private def rotate(direction: Int) = controller.rotate(playerId, direction)
  
  def getKeyboardBinding(style: Player.Keys): Key.Value => (() => Unit) = {
    style match {
      case Player.RightKeys => (key: Key.Value) => {
        key match {
          case Key.Right => moveRight _ 
          case Key.Left => moveLeft _
          case Key.Down => moveDown _
          case Key.Up => rotateRight _
          case Key.Control => moveInstantlyDown _
          case Key.P =>  pause _
          case _ => null
        }
      }
      case Player.LeftKeys => (key: Key.Value) => {
        key match {
          case Key.D => moveRight _
          case Key.A => moveLeft _
          case Key.S => moveDown _
          case Key.W => rotateRight _
          case Key.X => moveInstantlyDown _
          case _ => null
        }
      }
    }
  }
}