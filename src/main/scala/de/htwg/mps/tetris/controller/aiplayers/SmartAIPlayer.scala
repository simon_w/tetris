package de.htwg.mps.tetris.controller.aiplayers

import de.htwg.mps.tetris.model.Model
import de.htwg.mps.tetris.controller.Controller
import de.htwg.mps.tetris.controller.Player
import de.htwg.mps.tetris.model.SingleGameModel
import de.htwg.mps.tetris.model.Block
import scala.actors.Actor._
import de.htwg.mps.tetris.model.Grid

class SmartAIPlayer(controller: Controller, playerId: Int, strength: Int = 1, speed: Int = 1) extends Player(controller, playerId) {

  class Coords(val posX: Int, val orientation: Int) {
    override def toString = posX + "/" + orientation
  }

  val possibleBlockCoordinates = for (posX <- -2 until ownModel.grid.width + 2; orientation <- 0 to 3)
    yield new Coords(posX, orientation)

  def allowedBlockCoordinates(block: Block) = {
    possibleBlockCoordinates.filter(coords => {
      block.posX = coords.posX
      block.itsRotation = coords.orientation
      ownModel.checkIfBlockCoordinatesAreAllowed(block)
    })
  }

  def calculateBestCoordinates(): Coords = {

    val modelCopy = ownModel.copy

    val allowedCoords = allowedBlockCoordinates(modelCopy.currentBlock)

    val values = for (coords <- allowedCoords) yield {
      val modelCopy = ownModel.copy
      modelCopy.currentBlock.posX = coords.posX
      modelCopy.currentBlock.itsRotation = coords.orientation
      modelCopy.moveInstantlyDownAndPersistBlock
      val points = valuate(modelCopy)
      //println(points + " " + modelCopy.completedRowsCount + ":\n" + modelCopy)
      points
    }
    //println("points:" + values)

    val bestSolution = values.zipWithIndex.reduce((a, b) => if (a._1 > b._1) a else b)
//    //println("best:" + bestSolution)

    allowedCoords(bestSolution._2)
  }

  def valuate(model: SingleGameModel) = {
    val height = blocksHeight(model.grid)
    
    height            			* -10 +
    model.completedRowsCount 	* 500 +
    unreachableSquares(model)   * -40
    //gaps(model.grid, height)    * -40
  }

  def blocksHeight(grid: Grid) : Int = {
    var firstRowNb = -1
    for (rowNb <- 0 until grid.height if firstRowNb == -1) {
      if (!grid.squares(rowNb).forall(square => square.squareType == 0)) firstRowNb = rowNb
    }
    grid.height - firstRowNb
  }
  
  def gaps(grid: Grid, height: Int) : Int = {
    var gapNb = 0
    for(y <- grid.height - height until grid.height; x <- 0 until grid.width) {
      
      //todo: use some sort of floodfill
      //println(x + "/" + y)
      // if cell is surrounded by borders or persisted squares
      if(grid.squares(y)(x).squareType == 0 && 
          (x+1 == grid.width || grid.squares(y)(x).squareType >= 1) &&
          (x-1 == 0          || grid.squares(y)(x-1).squareType >= 1) &&
          (y+1 == grid.height || grid.squares(y+1)(x).squareType >= 1) &&
          grid.squares(y-1)(x).squareType >= 1)
      gapNb += 1
    }
    println(gapNb)
    gapNb
  }
  
  def unreachableSquares(model: SingleGameModel) : Int = {
    var nb = 0
    for(x <- 0 until model.grid.width) {
      var y = 0
      while(y < model.grid.height && model.grid.squares(y)(x).squareType == 0) {
        y += 1
      }
      y += 1
      while(y < model.grid.height) {
        if(model.grid.squares(y)(x).squareType == 0) nb += 1
        y += 1
      }
    }
    nb
  }
  

  def getPlayerActionToReach(coords: Coords): () => Unit = {
    if (coords.orientation > currentBlock.itsRotation) rotateRight _
    else if (coords.orientation < currentBlock.itsRotation) rotateLeft _
    else if (coords.posX > currentBlock.posX) moveRight _
    else if (coords.posX < currentBlock.posX) moveLeft _
    else null
  }

  val playerActor = actor {
    while (!detached && !ownModel.gameOver) {
      // for each possible position and orientation of the current block, calculate some value of the changement
      // and resulting grid. pick the one with the highest rating and move to that position

      val knownBlock = currentBlock
      val coords = calculateBestCoordinates
      var movementFkt: () => Unit = null
      do {
        //println(coords + " " + currentBlock.posX + "/" + currentBlock.itsRotation)
        movementFkt = getPlayerActionToReach(coords)
        if (movementFkt != null) movementFkt()
        Thread.sleep(keyPressTime)
      } while (movementFkt != null)
      moveInstantlyDown

      while (currentBlock == knownBlock) {
        Thread.sleep(300)
      }
    }
  }
  
  def keyPressTime = {
    var time = 200 - speed * 10
    if(time < 10) time = 10
    println(playerId + " keyPressTime:" + time)
    time
  }

  def ownModel: SingleGameModel = controller.model.models(playerId)
  def currentBlock: Block = ownModel.currentBlock
}