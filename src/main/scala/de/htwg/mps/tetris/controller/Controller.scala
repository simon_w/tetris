package de.htwg.mps.tetris.controller
import scala.actors.Actor
import scala.reflect.Type
import scala.swing.event.Event
import de.htwg.mps.tetris.model.Model
import de.htwg.mps.tetris.model.SingleGameModel
import de.htwg.mps.tetris.view.View
import de.htwg.mps.tetris.controller.aiplayers.StupidAIPlayer
import de.htwg.mps.tetris.view.SwingView
import de.htwg.mps.tetris.Tetris
import de.htwg.mps.tetris.controller.aiplayers.SmartAIPlayer
import scala.collection.mutable.Map
import de.htwg.mps.tetris.view.swing.styles._

class Controller(var model: Model) extends Actor {

  var interval = 1000
  var started = false

  val publisher = new ControllerPublisher

  val timer = new Timer(interval)
  timer.addListener(this)

  val players = Map[Int, Player]()
  def registerPlayer(playerId: Int, player: Player) {
    if (players.contains(playerId)) players(playerId).detach
    players(playerId) = player
  }

  def startGame {
    this.start
    timer.start
    started = true
  }

  def pauseGame {
    started = false
    notifyViews
  }

  def newGame(gridWidth: Int = 10, gridHeight: Int = 20) {
    model = new Model(model.noOfPlayers, gridWidth, gridHeight)
    views.foreach(view => { view.reinit(this) })
    notifyViews
  }

  def togglePause {
    if (started) pauseGame else startGame
  }

  def gameRunning = started && !model.allGamesAreOver

  var views: List[View] = List()

  def addView(view: View) = views = view :: views;

  def notifyViews {
    views.foreach(view => { view.update })
    publisher.publishEvent(new TetrisModelChanged)
  }

  def notifyTimerListener = notifyViews

  def act = {
    loop {
      react {
        case timer.Timeout => {
          if (gameRunning) {
            model.timeout
            timer.setTimeout((model.models(0).scorer.shortenIntervalMultiplier * interval).toInt)
            notifyViews
          } else {
            //this ! timer.Stop 
          }
        }
        case timer.Stop => {
          timer ! timer.Stop
          exit
        }
      }
    }
  }

  def move(playerId: Int, direction: Int) {
    if (gameRunning) {
      model.updateBlock(playerId, dx = direction)
      notifyViews
    }
  }

  def rotate(playerId: Int, direction: Int) {
    if (gameRunning) {
      model.updateBlock(playerId, drotation = direction)
      notifyViews
    }
  }

  def moveDown(playerId: Int) {
    if (gameRunning) {
      model.updateBlock(playerId, dy = 1)
      notifyViews
    }
  }

  def moveInstantlyDown(playerId: Int) {
    if (gameRunning) {
      model.moveInstantlyDownAndPersistBlock(playerId)
      notifyViews
    }
  }

  def setNumberOfPlayers(number: Int) {
    model = new Model(number, 10, 20)
    views.foreach(view => { view.reinit(this) })
    notifyViews
  }

  def setPlayer(index: Int, player: Player) {
    players(index).detach
    players(index) = player
  }

  def mixinStyle(playerId: Int, styleId: Int) {
    if (model.models.length > playerId) {
      styleId match {
        case 0 => model.models(playerId).style = new StandardTetrisStyle with TetrisCompanyStyle
        case 1 => model.models(playerId).style = new StandardTetrisStyle with RedColors
        case 2 => model.models(playerId).style = new StandardTetrisStyle with BlueColors
        case 3 => model.models(playerId).style = new StandardTetrisStyle with TetrisCompanyStyle with Fill3DRectStyle
        case 4 => model.models(playerId).style = new StandardTetrisStyle with TetrisCompanyStyle with RoundRectStyle
        case 5 => model.models(playerId).style = new StandardTetrisStyle with RedColors with RoundRectStyle
        case 6 => model.models(playerId).style = new StandardTetrisStyle with BlueColors with RoundRectStyle
        case 7 => model.models(playerId).style = new StandardTetrisStyle with RedColors with Fill3DRectStyle
        case 8 => model.models(playerId).style = new StandardTetrisStyle with BlueColors with Fill3DRectStyle
      }
      views.foreach(view => { view.reinit(this) })
      notifyViews
    }
  }

  //  def setPlayer(index: Int, playerType: Int) {
  //    //match by classtype
  //    val gui = classOf[GuiPlayer]
  //    val stupidAi = classOf[StupidAIPlayer]
  //
  //    playerType match {
  //      case 0 => {
  //        //Guiplayer left
  //        model.players(index) = new GuiPlayer(Tetris.gui.panel, this, index, Player.LeftKeys)
  //      }
  //      case 1 => {
  //        //Guiplayer right
  //        model.players(index) = new GuiPlayer(Tetris.gui.panel, this, index, Player.RightKeys)
  //      }
  //      case 2 => {
  //        //StupidAi
  //        model.players(index) = new StupidAIPlayer(this, index)
  //      }
  //      case _ => println("unknown")
  //    }
  //  }

}
