package de.htwg.mps.tetris.controller

import scala.swing.MainFrame
import scala.swing.Component
import scala.swing.event.KeyPressed
import scala.swing.event.KeyReleased
import scala.swing.Frame
import scala.swing.Panel
import scala.swing.event.Key
import scala.collection.immutable.Map
import scala.swing.event.KeyPressed
import scala.swing.event.Event

class GuiPlayer(panel: Panel, controller: Controller, playerId: Int, keys: Player.Keys) extends Player(controller, playerId) {

  //val keyToAction = new Map[Key.Value, (=> Unit)](Key.Right => movewRight)
  
  
  
  /*def keyToAction(key: Key.Value) : (=> Unit) = {
    case Key.Right => moveRight
    case Key.Left => moveLeft
    case Key.Down => moveDown
    case Key.Up => rotateRight
    //case Key.Space => if (pressed) moveInstantlyDown
    //case _ => // do nothing
  }
  */
  
  val keyboardInput = new AutorepeatKeyListener(getKeyboardBinding(keys));
  
  panel.listenTo(panel.keys)
  
  //var x = PartialFunction[Event, Unit]()
  val keyReactions: PartialFunction[Event, Unit]  = {
    case e: KeyPressed  => keyboardInput.keyPressed(e)
    case e: KeyReleased => keyboardInput.keyReleased(e)
  }
  
  panel.reactions += keyReactions
  
  /*
  panel.reactions += {
    case e: KeyPressed  => keyboardInput.keyPressed(e)
    case e: KeyReleased => keyboardInput.keyReleased(e)
  }
  */
  
  override def detach = {
    println("DETACH")
    panel.deafTo(panel.keys)
    /*panel.reactions -= {
      case e: KeyPressed  => keyboardInput.keyPressed(e)
      case e: KeyReleased => keyboardInput.keyReleased(e)}
    */
    panel.reactions -= keyReactions
  }
}