package de.htwg.mps.tetris.controller

import scala.actors.Actor._
import scala.actors.Actor

class ConsolePlayer(controller: Controller, playerId: Int) extends Player(controller, playerId) {

  val playerActor : Actor = actor {
    
    // helper actor to avoid playerActor blocking itself
    actor {
      while(!detached) {
          val line = readLine()
    	  playerActor ! line
      }
    }
    
    loop {
      react {
        case "a" => moveLeft
        case "d" => moveRight
        case "s" => moveDown
        case "w" => rotateLeft
        case "e" => rotateRight
        case "x" => moveInstantlyDown
        case _ =>
      }
    }
  }
}