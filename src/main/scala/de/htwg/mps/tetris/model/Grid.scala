package de.htwg.mps.tetris.model

import scala.util.Random

class Grid(var width: Int, var height: Int) {

  var squares = Array.ofDim[Square](height, width)
  
  for (row <- 0 until height; col <- 0 until width)
    squares(row)(col) = new Square(0)

//  def generateTestCells = {
//    squares(height - 1)(0).squareType = 1;
//    squares(height - 1)(1).squareType = 1;
//    squares(height - 1)(2).squareType = 1;
//    squares(height - 1)(3).squareType = 1;
//    squares(height - 1)(4).squareType = 7;
//    squares(height - 1)(5).squareType = 7;
//    squares(height - 1)(6).squareType = 7;
//    squares(height - 2)(5).squareType = 7;
//  }

  def addLines(nb: Int) {
    // move all persisted block nb line(s) up 
    for (row <- nb until height; col <- 0 until width) {
      squares(row - nb)(col).squareType = squares(row)(col).squareType
    }

    // generate new lines with a random gap
    for (row <- height - nb until height) {
      val gap = Random.nextInt(width)
      for (col <- 0 until width) {
        squares(row)(col).squareType = if(col != gap) 8 else 0
      }
    }
  }
  
  def fill(){
    for (row <- 0 until height; col <- 0 until width) {
      squares(row)(col).squareType = 9 + Random.nextInt(2)
    }
  }

  override def toString = {
    /*
    var string = ""
    squares.foreach(row => {
      string += '|'
      row.foreach(square => string += square)
      string += '|'
      string += "\n"
    })
    string
    */
    (for (row <- squares) yield row.mkString("|", "", "|")).mkString("\n")
  }
}
