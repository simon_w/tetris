package de.htwg.mps.tetris.model

import scala.annotation.tailrec
import de.htwg.mps.tetris.view.swing.styles.TetrisCompanyStyle
import de.htwg.mps.tetris.view.swing.styles.Fill3DRectStyle
import de.htwg.mps.tetris.view.swing.styles.StandardTetrisStyle
import de.htwg.mps.tetris.view.swing.styles.StandardTetrisStyle

class SingleGameModel(gridWidth: Int, gridHeight: Int, var style: StandardTetrisStyle) {

  val grid = new Grid(gridWidth, gridHeight)

  def getGrid = grid

  var currentBlock = new Block
  setCurrentBlockPosition

  var nextBlock = new Block

  val scorer = new Scorer

  var gameOver = false

  var completedRowsCount = 0

  def copy: SingleGameModel = {
    val model = new SingleGameModel(gridWidth, gridHeight, style)
    model.currentBlock = currentBlock.copy
    model.nextBlock = nextBlock.copy
    for (row <- 0 until grid.height; col <- 0 until grid.width)
      model.grid.squares(row)(col).squareType = grid.squares(row)(col).squareType
    model.gameOver = gameOver
    model
  }

  private def setCurrentBlockPosition = {
    currentBlock.posX = (grid.width - currentBlock.shape.length) / 2
    currentBlock.posY = -currentBlock.currentUpperBound
  }

  def updateBlock(dx: Int, dy: Int, drotation: Int) = {
    if (!gameOver) {
      completedRowsCount = 0
      if (!checkIfCurrentBlockHitsBottom || dy <= 0) {
        if (checkIfCurrentBlockCanBeMoved(dx, dy, drotation))
          currentBlock.move(dx, dy, drotation)
        scorer.scoreMovedRows(dy)
      } else {
        persistAndContinue
      }
    }
  }

  def moveInstantlyDownAndPersistBlock = {
    if (!gameOver) {
      var countRows = 0
      while (!checkIfCurrentBlockHitsBottom) {
        currentBlock.move(0, 1, 0);
        countRows += 1
      }
      scorer.scoreMovedRows(countRows)
      persistAndContinue
    }
  }

  private def persistAndContinue = {
    persistBlock
    currentBlock = nextBlock.copy
    nextBlock = new Block
    setCurrentBlockPosition
    scorer.scoreBlock

    if (checkIfGameIsOver) {
      gameOver = true
      grid.fill()
    }
  }

  def checkIfGameIsOver = checkIfCurrentBlockHitsBottom

  override def toString = {

    val width = grid.width + 18
    val height = grid.height

    val rows = Array.fill[Char](height, width)(' ') // currying    //Array.ofDim[Char](height, width)

    // overlay a string with \n to our two-dim char array at specified position
    /*
    def overlayStringAtPosition(posX: Int, posY: Int, string: String) {
      var x, y = 0
      string.toCharArray.foreach(char => {
        if (char != '\n') {
          val absX = posX + x
          val absY = posY + y
          if (char != ' ' && absX >= 0 && absX < width && absY >= 0 && absY < height) rows(absY)(absX) = char
          x += 1
        } else {
          y += 1
          x = 0
        }
      })
    }
    */

    def overlayStringAtPosition(posX: Int, posY: Int, string: String) {
      val lines = string.split("\n")
      (lines, rows.drop(posY), posY until rows.size).zipped.foreach(
        (line, row, rowIndex) => (line.toCharArray(), row.drop(posX), posX until row.size).zipped.foreach(
          (newChar, _, colIndex) => if (newChar != ' ' && rowIndex >= 0 && colIndex >= 0) rows(rowIndex)(colIndex) = newChar))
    }

    // draw grid with persisted block squares
    overlayStringAtPosition(0, 0, grid.toString)

    // overlay current block definition to grid
    overlayStringAtPosition(currentBlock.posX + 1, currentBlock.posY, currentBlock.toString)

    // overlay next block definition to grid
    overlayStringAtPosition(grid.width + 4, 2, nextBlock.toString)

    // draw box around next block preview
    for (y <- 0 to 5) {
      if (y == 5 || y == 0) for (x <- 1 to 4) rows(1 + y)(grid.width + 3 + x) = '-'
      else {
        rows(1 + y)(grid.width + 3) = '|'
        rows(1 + y)(grid.width + 8) = '|'
      }
    }

    // write string "next"
    for (x <- 0 to 3) {
      val char = "next".toCharArray()(x)
      rows(0)(grid.width + 4 + x) = char
    }

    overlayStringAtPosition(grid.width + 4, 8, "score: %07d".format(scorer.score))
    overlayStringAtPosition(grid.width + 4, 9, "level: %02d".format(scorer.level))

    // return row array as string 
    /*
    var output = ""
    var rowNbOutput = 0
    rows.foreach(row => {
      output += rowNbOutput.formatted("%2s")
      row.foreach(char => output += { if (char != 0) char else " " })
      output += "\n"
      rowNbOutput = rowNbOutput + 1
    })
    
    output
    */

    @tailrec
    def buildOutput(output: String = "", rowNb: Int = 0): String = {
      if (rowNb == rows.length) output
      else buildOutput(output + rowNb.formatted("%2s") + rows(rowNb).mkString + "\n", rowNb + 1)
    }

    buildOutput()
  }

  def checkIfCurrentBlockHitsBottom: Boolean = {
    for (x <- 0 to 3; y <- 0 to 3) {
      var absX = currentBlock.posX + x
      var absY = currentBlock.posY + y + 1
      if (currentBlock.shape(currentBlock.itsRotation)(y)(x) == 1 && absY >= 0 && absX >= 0 && absX < gridWidth && 
        (absY >= grid.height || grid.squares(absY)(absX).squareType > 0)) {
        return true
      }
    }
    false
  }

  def checkIfCurrentBlockCanBeMoved(dx: Int, dy: Int, drotation: Int): Boolean = {
    val currentblockClone = currentBlock.copy()
    currentblockClone.move(dx, dy, drotation)
    checkIfBlockCoordinatesAreAllowed(currentblockClone)
  }

  def checkIfBlockCoordinatesAreAllowed(block: Block): Boolean = {
    for (x <- 0 to 3; y <- 0 to 3) {
      val absX = block.posX + x
      val absY = block.posY + y
      // square is active for current shape and not inside bottom, left and right bounds or collides with persisted squares
      if (block.shape(block.itsRotation)(y)(x) == 1 &&
        (absY >= grid.height || absX < 0 || absX >= grid.width ||
          (absY >= 0 && grid.squares(absY)(absX).squareType > 0))) {
        return false
      }
    }
    true
  }

  def persistBlock = {
    for (x <- 0 to 3; y <- 0 to 3) {
      val absX = currentBlock.posX + x
      val absY = currentBlock.posY + y
      if ((absX >= 0 && absX < grid.width && absY >= 0 && absY < grid.height) &&
        currentBlock.shape(currentBlock.itsRotation)(y)(x) == 1) {
        grid.squares(absY)(absX).squareType = currentBlock.squareType
      }
    }
    deleteCompletedRows
  }

  def deleteCompletedRows = {
    completedRowsCount = getCompletedRows.length
    scorer.scoreCompletedRows(getCompletedRows.length)
    while (getCompletedRows.length > 0) {
      deleteRow(getCompletedRows.head)
    }
  }

  def deleteRow(rowIndex: Int) = {
    if (rowIndex > 0) {
      for (y <- rowIndex to 2 by -1; x <- 0 until grid.width)
        grid.squares(y)(x).squareType = grid.squares(y - 1)(x).squareType
    }
    grid.squares(0).foreach(square => square.squareType = 0)
  }

  def getCompletedRows = {
    var completedRows: List[Int] = List()
    for (y <- 0 until grid.height) {
      if (grid.squares(y).forall(square => square.squareType != 0)) {
        completedRows = y :: completedRows
      }
    }
    completedRows
  }

  def getStyle(): StandardTetrisStyle = style

}
