package de.htwg.mps.tetris.model

/**
 * Definition of the six different block shapes in Tetris as nested lists
 * list[orientation 0-3][rows 0-3][cols 0-3]
 * 
 * @author Simon Weber, Maurizio Tidei
 */
object BlockShapes {

  val I = List(
    List(
      List(0,0,0,0),
      List(1,1,1,1),
      List(0,0,0,0),
      List(0,0,0,0)),
    List(
      List(0,1,0,0),
      List(0,1,0,0),
      List(0,1,0,0),
      List(0,1,0,0)),
    List(
      List(0,0,0,0),
      List(1,1,1,1),
      List(0,0,0,0),
      List(0,0,0,0)),
     List(
      List(0,1,0,0),
      List(0,1,0,0),
      List(0,1,0,0),
      List(0,1,0,0)))

  val O = List(
    List(
      List(0,0,0,0),
      List(0,1,1,0),
      List(0,1,1,0),
      List(0,0,0,0)),
    List(
      List(0,0,0,0),
      List(0,1,1,0),
      List(0,1,1,0),
      List(0,0,0,0)),
    List(
      List(0,0,0,0),
      List(0,1,1,0),
      List(0,1,1,0),
      List(0,0,0,0)),
    List(
      List(0,0,0,0),
      List(0,1,1,0),
      List(0,1,1,0),
      List(0,0,0,0)))

  val L = List(
    List(
      List(0,0,0,0),
      List(1,1,1,0),
      List(1,0,0,0),
      List(0,0,0,0)),
    List(
      List(0,1,0,0),
      List(0,1,0,0),
      List(0,1,1,0),
      List(0,0,0,0)),
    List(
      List(0,0,1,0),
      List(1,1,1,0),
      List(0,0,0,0),
      List(0,0,0,0)),
    List(
      List(1,1,0,0),
      List(0,1,0,0),
      List(0,1,0,0),
      List(0,0,0,0)))

  val J = List(
    List(
      List(0,0,0,0),
      List(1,1,1,0),
      List(0,0,1,0),
      List(0,0,0,0)),
    List(
      List(0,1,1,0),
      List(0,1,0,0),
      List(0,1,0,0),
      List(0,0,0,0)),
    List(
      List(1,0,0,0),
      List(1,1,1,0),
      List(0,0,0,0),
      List(0,0,0,0)),
    List(
      List(0,1,0,0),
      List(0,1,0,0),
      List(1,1,0,0),
      List(0,0,0,0)))

  val Z = List(
    List(
      List(0,0,0,0),
      List(1,1,0,0),
      List(0,1,1,0),
      List(0,0,0,0)),
    List(
      List(0,1,0,0),
      List(1,1,0,0),
      List(1,0,0,0),
      List(0,0,0,0)),
    List(
      List(0,0,0,0),
      List(1,1,0,0),
      List(0,1,1,0),
      List(0,0,0,0)),
    List(
      List(0,1,0,0),
      List(1,1,0,0),
      List(1,0,0,0),
      List(0,0,0,0)))

  val S = List(
    List(
      List(0,0,0,0),
      List(0,1,1,0),
      List(1,1,0,0),
      List(0,0,0,0)),
    List(
      List(1,0,0,0),
      List(1,1,0,0),
      List(0,1,0,0),
      List(0,0,0,0)),
    List(
      List(0,0,0,0),
      List(0,1,1,0),
      List(1,1,0,0),
      List(0,0,0,0)),
    List(
      List(1,0,0,0),
      List(1,1,0,0),
      List(0,1,0,0),
      List(0,0,0,0)))

  val T = List(
    List(
      List(0,0,0,0),
      List(1,1,1,0),
      List(0,1,0,0),
      List(0,0,0,0)),
    List(
      List(0,1,0,0),
      List(0,1,1,0),
      List(0,1,0,0),
      List(0,0,0,0)),
    List(
      List(0,1,0,0),
      List(1,1,1,0),
      List(0,0,0,0),
      List(0,0,0,0)),
    List(
      List(0,1,0,0),
      List(1,1,0,0),
      List(0,1,0,0),
      List(0,0,0,0)))

}