package de.htwg.mps.tetris.model
import scala.collection.mutable.HashMap
import scala.util.Random

class Block {
  val positions = 4
  var itsRotation = 0
  var posX = 0
  var posY = 0

  val blockMap = new HashMap[Int, List[List[List[Int]]]]()
  blockMap += 1 -> BlockShapes.I
  blockMap += 2 -> BlockShapes.O
  blockMap += 3 -> BlockShapes.L
  blockMap += 4 -> BlockShapes.J
  blockMap += 5 -> BlockShapes.Z
  blockMap += 6 -> BlockShapes.S
  blockMap += 7 -> BlockShapes.T

  var squareType = Random.nextInt(7) + 1
  var shape = blockMap(squareType)
  posX = 3

  def move(x: Int = 0, y: Int = 0, rotation: Int = 0) = {
    posX += x; posY += y
    itsRotation = (itsRotation + rotation) % positions
    if (itsRotation < 0) itsRotation += positions
  }
  
  def --> = move(x = 1) 
  def <-- = move(x = -1)

  def copy() = {
    var theCopy = new Block()
    theCopy.itsRotation = this.itsRotation
    theCopy.posX = this.posX
    theCopy.posY = this.posY
    theCopy.shape = this.shape
    theCopy.squareType = this.squareType
    theCopy
  }

//  def leftBound = {
//    var leftBounds: List[Int] = List()
//    shape(itsRotation).foreach(sublist => {
//      if (sublist.indexWhere(element => element != 0) >= 0)
//        leftBounds = sublist.indexWhere(element => element != 0) :: leftBounds
//    })
//    leftBounds.min
//  }
//
//  def rightBound = {
//    var rightBounds: List[Int] = List()
//    shape(itsRotation).foreach(sublist => {
//      if (sublist.indexWhere(element => element != 0) >= 0)
//        rightBounds = sublist.lastIndexWhere(element => element != 0) :: rightBounds
//    })
//    rightBounds.max
//  }

  def currentUpperBound: Int = {
    var upperBound = 0
    shape(itsRotation).foreach(sublist => {
      if (sublist.indexWhere(element => element != 0) >= 0) {
        return upperBound
      } else {
        upperBound += 1
      }
    })
    upperBound
  }

  override def toString = {
    var string = ""
    for (y <- 0 to 3; x <- 0 to 3) {
      if (shape(itsRotation)(y)(x) == 1)
        string += "X"
      else
        string += " "

      if (x == 3) string += "\n"
    }
    string
  }

  //override def toString() = "Block: (" + posX + "/" + posY + "), Pos: " + itsRotation
}
