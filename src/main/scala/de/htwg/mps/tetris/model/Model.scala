package de.htwg.mps.tetris.model
import de.htwg.mps.tetris.controller.Player
import scala.annotation.tailrec
import de.htwg.mps.tetris.view.swing.styles.StandardTetrisStyle
import de.htwg.mps.tetris.view.swing.styles.TetrisCompanyStyle
import de.htwg.mps.tetris.view.swing.styles.StandardTetrisStyle
import de.htwg.mps.tetris.view.swing.styles.TetrisCompanyStyle
import de.htwg.mps.tetris.view.swing.styles.Fill3DRectStyle
import de.htwg.mps.tetris.view.swing.styles.StandardTetrisStyle
import de.htwg.mps.tetris.view.swing.styles.RoundRectStyle
import de.htwg.mps.tetris.view.swing.styles.Fill3DRectStyle
import de.htwg.mps.tetris.view.swing.styles.BlueColors

class Model(val noOfPlayers: Int = 1, gridWidth: Int, gridHeight: Int) {

  val models = Array.ofDim[SingleGameModel](noOfPlayers)

  for (i <- 0 until noOfPlayers) {
    models(i) = new SingleGameModel(gridWidth, gridHeight, 
    i match{
      case 0 => new StandardTetrisStyle with TetrisCompanyStyle
      case 1 => new StandardTetrisStyle with BlueColors with Fill3DRectStyle
      case _ => new StandardTetrisStyle with Fill3DRectStyle
    })
  }

  def timeout = {
    for (model <- models if !model.gameOver) {
      model.updateBlock(0, 1, 0)
      //handleMultiplayerLines(playerId)
    }
  }

  def allGamesAreOver: Boolean = models.forall(model => model.gameOver)

  def updateBlock(playerId: Int, dx: Int = 0, dy: Int = 0, drotation: Int = 0) = {
    models(playerId).updateBlock(dx, dy, drotation)
    handleMultiplayerLines(playerId)
  }

  def moveInstantlyDownAndPersistBlock(playerId: Int) {
    models(playerId).moveInstantlyDownAndPersistBlock
    handleMultiplayerLines(playerId)
  }

  def handleMultiplayerLines(playerId: Int) {
    if (models(playerId).completedRowsCount > 1) {
      for (model <- models; if model != models(playerId)) {
        model.grid.addLines(models(playerId).completedRowsCount - 1)
      }
    }
  }

  override def toString = {
    
    val lineLists = for (model <- models) yield model.toString.split("\n")
    /*
    var output = ""
    for (line <- 0 until lineLists(0).size) {
      for (list <- lineLists)
        output += list(line) + "   "
      output += "\n"
    }
    output
    */

    @tailrec
    def buildOutput(output: String = "", lineIndex: Int = 0) : String = {
      
      @tailrec
      def buildLine(line: String = "", listIndex: Int = 0) : String = {
        if (listIndex == lineLists.size) line + "\n"
        else buildLine(line + lineLists(listIndex)(lineIndex) + "   ", listIndex + 1)
      }
      
      if (lineIndex == lineLists(0).size) output
      else buildOutput(output + buildLine(), lineIndex + 1)
    }

    buildOutput()
  }
}

