package de.htwg.mps.tetris.model

class Scorer {

  private var _score = 0
  private var _level = 1
  private var _rowCount = 0
  private var blockCount = 0
  
  def shortenIntervalMultiplier = 1.0/_level
  def resetScore = {
    _score = 0
    _rowCount = 0
    _level = 1
  }

  def score = _score
  def rows = _rowCount
  def level = _level

  def scoreBlock = {
    blockCount += 1
    _level = (blockCount / ScoringConfiguration.levelUpAfterBlocks) + 1
  }

  def scoreCompletedRows(numberOfRows: Int) = {
    _score += ScoringConfiguration.scorePerCompletedRow * numberOfRows
    _rowCount += numberOfRows
    if (numberOfRows > 1) {
      _score += ScoringConfiguration.bonusPerCompletedMultiRows * numberOfRows
    }
  }

  def scoreMovedRows(numberOfRows: Int) = {
    _score += ScoringConfiguration.scorePerMovedRow * numberOfRows
    if (numberOfRows > 1) {
      _score += ScoringConfiguration.bonusPerInstantlyMovedRow * numberOfRows
    }
  }

  object ScoringConfiguration {
    val levelUpAfterBlocks = 20
    val scorePerCompletedRow = 10
    val bonusPerCompletedMultiRows = 10
    val scorePerMovedRow = 1
    val bonusPerInstantlyMovedRow = 2

    /*
     * 1x1 = 10
     * 2x1 = 20
     * ...
     * 
     * 1x2 = 40
     * 1x3 = 60
     * 1x4 = 80
     * ...
     */
  }
}
