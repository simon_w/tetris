package de.htwg.mps.tetris.model

class Square (var squareType: Int = 0) {
  
  override def toString =
    squareType match {
      case 0 => " "
      case _ => "#"
    }

}