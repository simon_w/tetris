package de.htwg.mps.tetris.view.swing.styles
import java.awt.Color
import java.awt.Graphics2D

class StandardTetrisStyle extends TetrisStyle {

  def getColor(squareType: Int): Color = {
    squareType match {
      case _ => Color.GRAY
    }
  }

  def getBackgroundColor() = Color.LIGHT_GRAY
  def getGridBorderColor() = Color.BLACK
  def getSquareBorderColor() = Color.WHITE

  /**
   * Draws a Rectangle
   */
  def drawRect(graphics: Graphics2D, x: Int, y: Int, squareType: Int, squareWidth: Int, squareHeight: Int) {
    graphics.setColor(getColor(squareType))
    graphics.fillRect(x * squareWidth + 1, y * squareWidth + 1, squareWidth, squareHeight)
    graphics.setColor(getSquareBorderColor())
    graphics.drawRect(x * squareWidth + 1, y * squareWidth + 1, squareWidth, squareHeight)
  }

}