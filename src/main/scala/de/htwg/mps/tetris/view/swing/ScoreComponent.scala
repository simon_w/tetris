package de.htwg.mps.tetris.view.swing

import java.awt.Graphics2D
import de.htwg.mps.tetris.model.SingleGameModel
import java.awt.Color
import scala.swing.Component
import java.awt.Font

class ScoreComponent(model: SingleGameModel) extends Component {

  val labelFont = new Font("Arial", Font.BOLD, 14)
  val valueFont = new Font("Arial", 0, 14)

  override def paintComponent(g: Graphics2D) = {

    // draw background and border
    g.setColor(Color.GRAY)
    g.fillRect(0, 0, size.width, size.height)
    g.setColor(Color.BLACK)
    g.drawRect(0, 0, size.width - 1, size.height - 1)
    
    g.setColor(Color.WHITE)
    
    g.setFont(labelFont)
    g.drawString("score:", 5, 20)
    g.drawString("level:", 5, 60)
    g.drawString("lines:", 5, 100)
    
    g.setFont(valueFont)
    g.drawString("%07d".format(model.scorer.score), size.width/2 - 5, 35)
    g.drawString("%02d".format(model.scorer.level), size.width - 19, 75)
    g.drawString("%04d".format(model.scorer.rows), size.width - 35, 115)
  }

}