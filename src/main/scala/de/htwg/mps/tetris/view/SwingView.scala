package de.htwg.mps.tetris.view

import java.awt.Rectangle
import scala.swing.Dimension
import scala.swing.MainFrame
import de.htwg.mps.tetris.model.SingleGameModel
import de.htwg.mps.tetris.view.swing.NullPanel
import de.htwg.mps.tetris.model.Model
import de.htwg.mps.tetris.controller.Controller
import scala.swing.MenuBar
import scala.swing.Menu
import scala.swing.event.Key
import scala.swing.MenuItem
import scala.swing.Action
import scala.swing.Dialog
import de.htwg.mps.tetris.controller.GuiPlayer
import de.htwg.mps.tetris.controller.Player
import de.htwg.mps.tetris.controller.aiplayers.SmartAIPlayer
import de.htwg.mps.tetris.controller.aiplayers.StupidAIPlayer
import de.htwg.mps.tetris.controller.aiplayers.MasochisticAIPlayer
import scala.collection.immutable.List
import scala.swing.Separator

class SwingView(var controller: Controller) extends View(controller) {

  val tetrisPanelSize = new Dimension(400, 600)

  val mainWindow = new MainFrame
  val title = "TETRIS - MPS"
  mainWindow.title = title

  var tetrisPanels = Array.ofDim[TetrisPanel](controller.model.noOfPlayers)

  for (i <- 0 until controller.model.noOfPlayers) {
    tetrisPanels(i) = new TetrisPanel(controller.model.models(i), tetrisPanelSize, controller.model.models(i).getStyle())
  }

  var panel = new NullPanel {
    //background = Color.BLUE

    // create a view for each model
    for (i <- 0 until tetrisPanels.size) {
      add(tetrisPanels(i), new Rectangle(tetrisPanelSize.width * i, 0, tetrisPanelSize.width, tetrisPanelSize.height))
    }

    focusable = true
    requestFocus
  }
  mainWindow.contents = panel
  mainWindow.size = new Dimension(tetrisPanelSize.width * tetrisPanels.size + 20, 600)
  mainWindow.menuBar = new MenuBar {
    contents += new Menu("File") {
      mnemonic = Key.F
      contents += new MenuItem(Action("New") { controller.newGame() })
      contents += new MenuItem(Action("Pause") { controller.togglePause })
      contents += new MenuItem(Action("Start") { controller.startGame })
      contents += new Separator
      contents += new MenuItem(Action("Quit") { System.exit(0) })
    }
    contents += new Menu("View") {
      mnemonic = Key.V
      for (no <- 1 to 4) {
        contents += new Menu("Player " + no) {
          getPlayerViewOptions(no - 1).foreach(contents += _)
        }
      }
    }
    contents += new Menu("Options") {
      mnemonic = Key.O
      contents += new Menu("Number of Players") {
        for (no <- 1 to 4) {
          contents += new MenuItem(Action(no.toString) { controller.setNumberOfPlayers(no) })
        }
      }
      contents += new Separator
      for (no <- 1 to 4) {
        contents += new Menu("Player " + no) {
          getPlayerOptions(no - 1).foreach(contents += _)
        }
      }
      contents += new Separator
      contents += new Menu("Tetris Grid Size") {
        contents += new MenuItem(Action("10x20 (default)") { controller.newGame(); mainWindow.repaint })
        contents += new MenuItem(Action("1x7") { controller.newGame(1, 7); mainWindow.repaint })
        contents += new MenuItem(Action("5x12") { controller.newGame(5, 12); mainWindow.repaint })
        contents += new MenuItem(Action("20x30") { controller.newGame(20, 32); mainWindow.repaint })
        contents += new MenuItem(Action("100x150") { controller.newGame(100, 150); mainWindow.repaint })
      }
    }
    contents += new Menu("Help") {
      mnemonic = Key.H
      contents += new MenuItem(Action("About") {
        Dialog.showMessage(
          message = "A project by\nSimon Weber & Maurizio Tidei\nHTWG MPS WS12/13",
          title = "About")
      })
    }
  }

  private def getPlayerOptions(playerId: Int): List[MenuItem] = {
    List(
      new MenuItem(Action("Human - ASDW Keys") { new GuiPlayer(panel, controller, playerId, Player.LeftKeys) }),
      new MenuItem(Action("Human - Arrow Keys") { new GuiPlayer(panel, controller, playerId, Player.RightKeys) }),
      new MenuItem(Action("AI Stupid") { new StupidAIPlayer(controller, playerId) }),
      new MenuItem(Action("AI Masochistic") { new MasochisticAIPlayer(controller, playerId) }),
      new MenuItem(Action("AI Smart") { new SmartAIPlayer(controller, playerId) }),
      new MenuItem(Action("AI Smart on Steroids") { new SmartAIPlayer(controller, playerId, speed = 10) }),
      new MenuItem(Action("AI Superhero") { new SmartAIPlayer(controller, playerId, speed = 20) }))
  }

  private def getPlayerViewOptions(playerId: Int): List[MenuItem] = {
    List(
      new MenuItem(Action("GUI with TetrisCompanyColors") { controller.mixinStyle(playerId, 0) }),
      new MenuItem(Action("GUI with RedColors") { controller.mixinStyle(playerId, 1) }),
      new MenuItem(Action("GUI with BlueColors") { controller.mixinStyle(playerId, 2) }),
      new MenuItem(Action("GUI with TetrisCompanyColors 3D") { controller.mixinStyle(playerId, 3) }),
      new MenuItem(Action("GUI with TetrisCompanyColors RoundRect") { controller.mixinStyle(playerId, 4) }),
      new MenuItem(Action("GUI with RedColors RoundRect") { controller.mixinStyle(playerId, 5) }),
      new MenuItem(Action("GUI with BlueColors RoundRect") { controller.mixinStyle(playerId, 6) }),
      new MenuItem(Action("GUI with RedColors 3D") { controller.mixinStyle(playerId, 7) }),
      new MenuItem(Action("GUI with BlueColors 3D") { controller.mixinStyle(playerId, 8) }))
  }

  mainWindow.open

  override def update = {
    tetrisPanels.foreach(tetrisPanel => tetrisPanel.update)
    if (!controller.gameRunning) mainWindow.title = title + " *PAUSED*" else mainWindow.title = title
  }

  def reinit(controller: Controller) {
    this.controller = controller
    tetrisPanels = Array.ofDim[TetrisPanel](controller.model.noOfPlayers)

    for (i <- 0 until controller.model.noOfPlayers) {
      tetrisPanels(i) = new TetrisPanel(controller.model.models(i), tetrisPanelSize, controller.model.models(i).getStyle())
    }

    //remove components
    panel.removeAllComponents

    //add components of modified tetris panels
    for (i <- 0 until tetrisPanels.size) {
      panel.add(tetrisPanels(i), new Rectangle(tetrisPanelSize.width * i, 0, tetrisPanelSize.width, tetrisPanelSize.height))
    }

    //panel.repaint()

    //mainWindow.contents = panel
    mainWindow.size = new Dimension(tetrisPanelSize.width * tetrisPanels.size + 20, 600)
  }
}