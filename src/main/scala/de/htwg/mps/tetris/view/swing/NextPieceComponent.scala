package de.htwg.mps.tetris.view.swing

import java.awt.Graphics2D
import de.htwg.mps.tetris.model.SingleGameModel
import java.awt.Color
import scala.swing.Component
import de.htwg.mps.tetris.view.swing.styles.TetrisCompanyStyle
import de.htwg.mps.tetris.view.swing.styles.TetrisCompanyStyle
import de.htwg.mps.tetris.view.swing.styles.YellowSquareBorderStyle
import de.htwg.mps.tetris.view.swing.styles.RoundRectStyle
import de.htwg.mps.tetris.view.swing.styles.Fill3DRectStyle
import de.htwg.mps.tetris.view.swing.styles.StandardTetrisStyle

class NextPieceComponent(model: SingleGameModel, style:StandardTetrisStyle) extends Component {

  var squareHeight, squareWidth = 0;

  override def paintComponent(g: Graphics2D) = {

    // draw background and border
    g.setColor(style.getBackgroundColor())
    g.fillRect(0, 0, size.width, size.height)
    g.setColor(style.getGridBorderColor())
    g.drawRect(0, 0, size.width - 1, size.height - 1)

    // draw next block
    for (y <- 0 to 3; x <- 0 to 3) {
      if (model.nextBlock.shape(model.nextBlock.itsRotation)(y)(x) == 1) style.drawRect(g, x, y, model.nextBlock.squareType, squareWidth, squareHeight)
    }
  }

  def resized = {
    squareHeight = (size.height - 3) / 4;
    squareWidth  = (size.width - 3)  / 4;
  }

}