package de.htwg.mps.tetris.view.swing

import java.awt.Graphics2D
import de.htwg.mps.tetris.model.SingleGameModel
import java.awt.Color
import scala.swing.Component
import de.htwg.mps.tetris.view.swing.styles.StandardTetrisStyle

class GridComponent(model: SingleGameModel, style: StandardTetrisStyle) extends Component {

  var squareHeight, squareWidth = 0

  override def paintComponent(g: Graphics2D) = {

    // draw background and border
    g.setColor(style.getBackgroundColor())
    g.fillRect(0, 0, size.width, size.height)
    g.setColor(style.getGridBorderColor())
    g.drawRect(0, 0, size.width - 1, size.height - 1)

    // draw persisted blocks  
    for (y <- 0 until model.grid.height; x <- 0 until model.grid.width) {
      val square = model.grid.squares(y)(x)
      if (square.squareType > 0) style.drawRect(g, x, y, square.squareType, squareWidth, squareHeight)
    }

    // draw falling block if not game over
    if (!model.gameOver) {
      for (y <- 0 to 3; x <- 0 to 3) {
        if (model.currentBlock.shape(model.currentBlock.itsRotation)(y)(x) == 1) style.drawRect(g, x + model.currentBlock.posX, y + model.currentBlock.posY, model.currentBlock.squareType, squareWidth, squareHeight)
      }
    }
  }

  def resized = {
    squareHeight = (size.height - 3) / model.grid.height;
    squareWidth = (size.width - 3) / model.grid.width;
  }

}