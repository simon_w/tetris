package de.htwg.mps.tetris.view.swing.styles
import java.awt.Color
import java.awt.Graphics2D
import java.awt.BasicStroke

trait TetrisCompanyStyle extends TetrisStyle {

  abstract override def getColor(squareType: Int): Color = {
    squareType match {
      case 1 => Color.CYAN
      case 2 => Color.YELLOW
      case 3 => Color.ORANGE
      case 4 => Color.BLUE
      case 5 => Color.RED
      case 6 => Color.GREEN
      case 7 => new Color(160, 0, 255) // PURPLE
      case 8 => new Color(180, 180, 180) // penalty rows
      case 9 => new Color(50, 50, 50) // for game over
      case 10 => new Color(70, 70, 70)
      case _ => Color.BLACK
    }
  }

  abstract override def getBackgroundColor(): Color = Color.GRAY

  abstract override def getGridBorderColor(): Color = Color.LIGHT_GRAY

  abstract override def getSquareBorderColor(): Color = Color.WHITE

}

trait RedColors extends TetrisStyle {

  abstract override def getColor(squareType: Int): Color = {
    squareType match {
      case 1 => new Color(255, 0, 0)
      case 2 => new Color(255, 69, 0)
      case 3 => new Color(255, 127, 0)
      case 4 => new Color(255, 165, 0)
      case 5 => new Color(255, 215, 0)
      case 6 => new Color(255, 255, 0)
      case 7 => new Color(255, 255, 60)
      case 8 => new Color(180, 180, 180) // penalty rows
      case 9 => new Color(50, 0, 0) // for game over
      case 10 => new Color(70, 0, 0)
      case _ => Color.BLACK
    }
  }

  abstract override def getBackgroundColor(): Color = Color.GRAY

  abstract override def getGridBorderColor(): Color = Color.LIGHT_GRAY

  abstract override def getSquareBorderColor(): Color = Color.WHITE
}

trait BlueColors extends TetrisStyle {

  abstract override def getColor(squareType: Int): Color = {
    squareType match {
      case 1 => new Color(0, 0, 255)
      case 2 => new Color(0, 69, 255)
      case 3 => new Color(0, 127, 255)
      case 4 => new Color(0, 165, 255)
      case 5 => new Color(0, 215, 255)
      case 6 => new Color(0, 255, 255)
      case 7 => new Color(60, 255, 255)
      case 8 => new Color(180, 180, 180) // penalty rows
      case 9 => new Color(0, 0, 50) // for game over
      case 10 => new Color(0, 0, 70)
      case _ => Color.BLACK
    }
  }

  abstract override def getBackgroundColor(): Color = Color.GRAY

  abstract override def getGridBorderColor(): Color = Color.LIGHT_GRAY

  abstract override def getSquareBorderColor(): Color = Color.WHITE
}

trait YellowSquareBorderStyle extends TetrisStyle {

  abstract override def getSquareBorderColor(): Color = Color.RED
}

trait RoundRectStyle extends TetrisStyle {
  abstract override def drawRect(graphics: Graphics2D, x: Int, y: Int, squareType: Int, squareWidth: Int, squareHeight: Int) {
    graphics.setColor(getColor(squareType))
    graphics.fillRoundRect(x * squareWidth + 1, y * squareWidth + 1, squareWidth, squareHeight, 10, 10)
    graphics.setColor(getSquareBorderColor())
    graphics.drawRoundRect(x * squareWidth + 1, y * squareWidth + 1, squareWidth, squareHeight, 10, 10)
  }
}

trait Fill3DRectStyle extends TetrisStyle {
  abstract override def drawRect(graphics: Graphics2D, x: Int, y: Int, squareType: Int, squareWidth: Int, squareHeight: Int) {
    graphics.setColor(getColor(squareType))
    graphics.fill3DRect(x * squareWidth + 1, y * squareWidth + 1, squareWidth, squareHeight, true)
    graphics.draw3DRect(x * squareWidth + 1, y * squareWidth + 1, squareWidth, squareHeight, true)
  }
}
