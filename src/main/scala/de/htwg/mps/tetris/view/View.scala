package de.htwg.mps.tetris.view

import de.htwg.mps.tetris.controller.Controller


abstract class View(controller: Controller) {

  def update
  def reinit(controller: Controller)
} 