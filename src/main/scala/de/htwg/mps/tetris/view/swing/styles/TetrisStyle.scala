package de.htwg.mps.tetris.view.swing.styles
import java.awt.Color
import java.awt.Graphics2D

abstract class TetrisStyle {
  def getColor(squareType: Int): Color
  def getBackgroundColor(): Color
  def getGridBorderColor(): Color
  def getSquareBorderColor(): Color

  def drawRect(graphics: Graphics2D, x: Int, y: Int, squareType: Int, squareWidth: Int, squareHeight: Int)
}