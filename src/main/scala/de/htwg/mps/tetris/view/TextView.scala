package de.htwg.mps.tetris.view

import de.htwg.mps.tetris.controller.Controller


class TextView(var controller: Controller) extends View(controller) {

  def update = println(controller.model)
  
  def reinit(controller: Controller){
    this.controller = controller
  }

}
