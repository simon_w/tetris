package de.htwg.mps.tetris.view

import java.awt.Dimension
import java.awt.Rectangle
import scala.swing.Alignment
import scala.swing.Label
import de.htwg.mps.tetris.model.SingleGameModel
import de.htwg.mps.tetris.view.swing.GridComponent
import de.htwg.mps.tetris.view.swing.NextPieceComponent
import de.htwg.mps.tetris.view.swing.NullPanel
import de.htwg.mps.tetris.view.swing.ScoreComponent
import de.htwg.mps.tetris.view.swing.styles.StandardTetrisStyle

class TetrisPanel(model: SingleGameModel, size: Dimension, style:StandardTetrisStyle) extends NullPanel {
  
  val singleCellSize = size.width / (model.grid.width + 4 + 1)
  
  val gridWidth  = singleCellSize * model.grid.width  + 3
  val gridHeight = singleCellSize * model.grid.height + 3
  
  val nextPieceSize = singleCellSize * 4 + 3
  
  val gridComponent = new GridComponent(model, style) {
    size = new Dimension(gridWidth, gridHeight)
  }
  gridComponent.resized
  
  val nextPieceComponent = new NextPieceComponent(model, style) {
    size = new Dimension(singleCellSize * 4 + 3, singleCellSize * 4 + 3)
  }
  nextPieceComponent.resized
  
  val nextPieceLabel = new Label("Next Piece")
  nextPieceLabel.horizontalAlignment = Alignment.Left
  
  val scoreComponent = new ScoreComponent(model)
  
  add(gridComponent, 		new Rectangle(10, 10, gridWidth, gridHeight))
  add(nextPieceLabel,  		new Rectangle(10 + gridWidth + 10, 10, nextPieceSize, 15))
  add(nextPieceComponent, 	new Rectangle(10 + gridWidth + 10, 30, nextPieceSize, nextPieceSize))
  add(scoreComponent, 		new Rectangle(10 + gridWidth + 10, 30 + nextPieceSize + 10, nextPieceSize, nextPieceSize + 20))
  
  def update = {
    // update the grid
    gridComponent.repaint

    // update the score box
    scoreComponent.repaint

    // update the next block box
    nextPieceComponent.repaint
  }
}