package de.htwg.mps.tetris.controller
import org.specs2.mutable.SpecificationWithJUnit
import de.htwg.mps.tetris.model.Grid
import de.htwg.mps.tetris.model.SingleGameModel
import de.htwg.mps.tetris.model.BlockShapes
import de.htwg.mps.tetris.model.Block
import de.htwg.mps.tetris.model.Model

class PlayerSpec extends SpecificationWithJUnit {

  class MockPlayer(controller: Controller, playerId: Int) extends Player(controller, playerId);

  "A new player" should {
    
    val controller = new Controller(new Model(noOfPlayers = 1, gridWidth = 10, gridHeight = 10))
    val player = new MockPlayer(controller, 0)
    val model = controller.model.models(0)
    controller.started = true
    
    sequential

    "be able to move the falling I-shape to the left" in {
      model.currentBlock = new Block
      model.currentBlock.posX = 1
      model.currentBlock.posY = 1
      model.currentBlock.shape = BlockShapes.I
      val xPreMove = model.currentBlock.posX
      player.moveLeft
      (model.currentBlock.posX - xPreMove) must be_==(-1)
    }

    "be able to move the falling I-shape to the right" in {
      model.currentBlock = new Block
      model.currentBlock.posX = 1
      model.currentBlock.posY = 1
      model.currentBlock.shape = BlockShapes.I
      val xPreMove = model.currentBlock.posX
      player.moveRight
      (model.currentBlock.posX - xPreMove) must be_==(1)
    }

    
    "be able to move the falling I-shape down" in {
      model.currentBlock = new Block
      model.currentBlock.posX = 1
      model.currentBlock.posY = 1
      model.currentBlock.shape = BlockShapes.I
      val yPreMove = model.currentBlock.posY
      player.moveDown
      (model.currentBlock.posY - yPreMove) must be_==(1)
    }

    "be able to move the falling I-shape instantly down (hard drop)" in {
      model.currentBlock = new Block
      model.currentBlock.posX = 1
      model.currentBlock.posY = 1
      model.currentBlock.shape = BlockShapes.I

      //shape gets persisted immediately
      var yPreMove = model.currentBlock.posY
      if (yPreMove == 0)
        player.moveDown
      model.currentBlock.posY must not be_== (0)
      yPreMove = model.currentBlock.posY
      player.moveInstantlyDown
      yPreMove must beGreaterThan(model.currentBlock.posY)
    }

    "be able to rotate the falling I-shape clockwise" in {
      model.currentBlock = new Block
      model.currentBlock.posX = 2
      model.currentBlock.posY = 2
      model.currentBlock.shape = BlockShapes.I
      
      val preRotation: Int = model.currentBlock.itsRotation
      player.rotateLeft
      
      //normally:
      //((preRotation - 1) % model.currentBlock.positions) must be_==(model.currentBlock.itsRotation)
      //but modulo with negative numbers however doesn't work here
      
      var expected = preRotation-1
      if(expected<0){
        expected=model.currentBlock.positions+expected
      }
      (expected % model.currentBlock.positions) must be_==(model.currentBlock.itsRotation)
    }

    "be able to rotate the falling I-shape counterclockwise" in {
      model.currentBlock = new Block
      model.currentBlock.posX = 2
      model.currentBlock.posY = 2
      model.currentBlock.shape = BlockShapes.I
      
      val preRotation = model.currentBlock.itsRotation
      player.rotateRight
      ((preRotation + 1) % model.currentBlock.positions) must be_==(model.currentBlock.itsRotation)
    }
  }

}