package de.htwg.mps.tetris.controller

import org.specs2.mutable.SpecificationWithJUnit
import de.htwg.mps.tetris.model.Grid
import de.htwg.mps.tetris.model.SingleGameModel
import de.htwg.mps.tetris.view.TextView
import de.htwg.mps.tetris.model.BlockShapes
import de.htwg.mps.tetris.model.Block
import de.htwg.mps.tetris.model.Model
import aiplayers.StupidAIPlayer
import aiplayers.SmartAIPlayer
import de.htwg.mps.tetris.view.swing.styles.TetrisCompanyStyle
import de.htwg.mps.tetris.view.swing.styles.RedColors
import de.htwg.mps.tetris.view.swing.styles.BlueColors
import de.htwg.mps.tetris.view.swing.styles.Fill3DRectStyle
import de.htwg.mps.tetris.view.swing.styles.RoundRectStyle

class ControllerSpec extends SpecificationWithJUnit {

  "A new controller" should {
    val controller = new Controller(new Model(1, 10, 20))
    val singleModel = controller.model.models(0)

    controller.started=true
    
    sequential

    "listen to its timer" in {
      controller.timer.timerListeners must contain(controller)
    }

    "have interval greater than 0" in {
      controller.interval must beGreaterThan(0)
    }

    "have no view" in {
      controller.views must have size (0)
    }

    "have a model with a falling shape" in {
      singleModel.currentBlock must not beNull
    }

    "have a model with a next shape" in {
      singleModel.nextBlock must not beNull
    }
    
    "be able to move the falling I-shape down" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 1
      singleModel.currentBlock.posY = 1
      singleModel.currentBlock.shape = BlockShapes.I
      val yPreMove = singleModel.currentBlock.posY
      controller.moveDown(0)
      (singleModel.currentBlock.posY - yPreMove) must be_==(1)
    }

    "be able to move the falling I-shape to the left" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 1
      singleModel.currentBlock.posY = 1
      singleModel.currentBlock.shape = BlockShapes.I
      val xPreMove = singleModel.currentBlock.posX
      controller.move(0, -1)
      (singleModel.currentBlock.posX - xPreMove) must be_==(-1)
    }

    "be able to move the falling I-shape to the right" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 1
      singleModel.currentBlock.posY = 1
      singleModel.currentBlock.shape = BlockShapes.I
      val xPreMove = singleModel.currentBlock.posX
      controller.move(0, 1)
      (singleModel.currentBlock.posX - xPreMove) must be_==(1)
    }
    
    "be able to move the falling I-shape to the left" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 1
      singleModel.currentBlock.posY = 1
      singleModel.currentBlock.shape = BlockShapes.I
      val xPreMove = singleModel.currentBlock.posX
      controller.model.models(0).currentBlock.<--
      (singleModel.currentBlock.posX - xPreMove) must be_==(-1)
    }

    "be able to move the falling I-shape to the right" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 1
      singleModel.currentBlock.posY = 1
      singleModel.currentBlock.shape = BlockShapes.I
      val xPreMove = singleModel.currentBlock.posX
      controller.model.models(0).currentBlock.-->
      (singleModel.currentBlock.posX - xPreMove) must be_==(1)
    }

    "be able to move the falling O-shape to the left" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 1
      singleModel.currentBlock.posY = 1
      singleModel.currentBlock.shape = BlockShapes.O
      //println("o left pre: " + singleModel)
      val xPreMove = singleModel.currentBlock.posX
      controller.move(0, -1)
      //println("o left post: " + singleModel)
      (singleModel.currentBlock.posX - xPreMove) must be_==(-1)
    }

    "be able to move the falling L-shape to the left" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 1
      singleModel.currentBlock.posY = 1
      singleModel.currentBlock.shape = BlockShapes.L
      //println("l left pre: " + singleModel)
      val xPreMove = singleModel.currentBlock.posX
      controller.move(0, -1)
      //println("l left post: " + singleModel)
      (singleModel.currentBlock.posX - xPreMove) must be_==(-1)
      //singleModel.currentBlock.posX - xPreMove must be_==(-1)
    }

    "be able to move the falling J-shape to the left" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 1
      singleModel.currentBlock.posY = 1
      singleModel.currentBlock.shape = BlockShapes.J
      val xPreMove = singleModel.currentBlock.posX
      controller.move(0, -1)
      (singleModel.currentBlock.posX - xPreMove) must be_==(-1)
    }

    "be able to move the falling Z-shape to the left" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 1
      singleModel.currentBlock.posY = 1
      singleModel.currentBlock.shape = BlockShapes.Z
      val xPreMove = singleModel.currentBlock.posX
      controller.move(0, -1)
      (singleModel.currentBlock.posX - xPreMove) must be_==(-1)
    }

    "be able to move the falling S-shape to the left" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 1
      singleModel.currentBlock.posY = 1
      singleModel.currentBlock.shape = BlockShapes.S
      val xPreMove = singleModel.currentBlock.posX
      controller.move(0, -1)
      (singleModel.currentBlock.posX - xPreMove) must be_==(-1)
    }

    "be able to move the falling T-shape to the left" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 1
      singleModel.currentBlock.posY = 1
      singleModel.currentBlock.shape = BlockShapes.T
      val xPreMove = singleModel.currentBlock.posX
      controller.move(0, -1)
      (singleModel.currentBlock.posX - xPreMove) must be_==(-1)
    }

    "be able to move the falling I-shape instantly down" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 1
      singleModel.currentBlock.posY = 1
      singleModel.currentBlock.shape = BlockShapes.I

      //shape gets persisted immediately
      var yPreMove = singleModel.currentBlock.posY
      if (yPreMove == 0)
        controller.moveDown(0)
      singleModel.currentBlock.posY must not be_== (0)
      yPreMove = singleModel.currentBlock.posY
      controller.moveInstantlyDown(0)
      yPreMove must beGreaterThan(singleModel.currentBlock.posY)
    }

    "be able to rotate a falling I-shape counterclockwise" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 2
      singleModel.currentBlock.posY = 2
      singleModel.currentBlock.shape = BlockShapes.I

      val preRotation = singleModel.currentBlock.itsRotation
      controller.rotate(0, 1)
      ((preRotation + 1) % singleModel.currentBlock.positions) must be_==(singleModel.currentBlock.itsRotation)
    }

    "be able to rotate a falling I-shape clockwise" in {
      singleModel.currentBlock = new Block
      singleModel.currentBlock.posX = 2
      singleModel.currentBlock.posY = 2
      singleModel.currentBlock.shape = BlockShapes.I

      val preRotation: Int = singleModel.currentBlock.itsRotation
      controller.rotate(0, -1)

      //normally:
      //((preRotation - 1) % singleModel.currentBlock.positions) must be_==(singleModel.currentBlock.itsRotation)

      var expected = preRotation - 1
      if (expected < 0) {
        expected = singleModel.currentBlock.positions + expected
      }
      (expected % singleModel.currentBlock.positions) must be_==(singleModel.currentBlock.itsRotation)
    }
    
    "be interruptible and continuable" in {
      controller.started must be_==(true)
      controller.pauseGame
      controller.started must be_==(false)
      controller.togglePause
      controller.started must be_==(true)
      controller.togglePause
      controller.started must be_==(false)
      controller.startGame
      controller.started must be_==(true)
    }
    
    "be able to generate a new game with standard grid size" in {
      controller.newGame()
      controller.model.models(0).getGrid.height must be_==(20)
      controller.model.models(0).getGrid.width must be_==(10)
    }
    
    "accept different number of players" in {
      controller.model.models.length must be_==(1)
      controller.setNumberOfPlayers(2)
      controller.model.models.length must be_==(2)
      controller.setNumberOfPlayers(1)
      controller.model.models.length must be_==(1)
    }
    
    "accept different style mixins" in {
      controller.mixinStyle(0, 0)
      controller.model.models(0).style.isInstanceOf[TetrisCompanyStyle] must be_==(true)
      
      controller.mixinStyle(0, 1)
      controller.model.models(0).style.isInstanceOf[RedColors] must be_==(true)
      
      controller.mixinStyle(0, 2)
      controller.model.models(0).style.isInstanceOf[BlueColors] must be_==(true)
      
      controller.mixinStyle(0, 3)
      controller.model.models(0).style.isInstanceOf[TetrisCompanyStyle] must be_==(true)
      controller.model.models(0).style.isInstanceOf[Fill3DRectStyle] must be_==(true)
      
      controller.mixinStyle(0, 4)
      controller.model.models(0).style.isInstanceOf[TetrisCompanyStyle] must be_==(true)
      controller.model.models(0).style.isInstanceOf[RoundRectStyle] must be_==(true)
      
      controller.mixinStyle(0, 5)
      controller.model.models(0).style.isInstanceOf[RedColors] must be_==(true)
      controller.model.models(0).style.isInstanceOf[RoundRectStyle] must be_==(true)
      
      controller.mixinStyle(0, 6)
      controller.model.models(0).style.isInstanceOf[BlueColors] must be_==(true)
      controller.model.models(0).style.isInstanceOf[RoundRectStyle] must be_==(true)
      
      controller.mixinStyle(0, 7)
      controller.model.models(0).style.isInstanceOf[RedColors] must be_==(true)
      controller.model.models(0).style.isInstanceOf[Fill3DRectStyle] must be_==(true)
      
      controller.mixinStyle(0, 8)
      controller.model.models(0).style.isInstanceOf[BlueColors] must be_==(true)
      controller.model.models(0).style.isInstanceOf[Fill3DRectStyle] must be_==(true)
    }
    
    "be able to exchange players" in {
      controller.setNumberOfPlayers(2)
      controller.model.models.size must be_==(2)
      controller.setPlayer(1, new StupidAIPlayer(controller, 1))
      controller.players(1).isInstanceOf[StupidAIPlayer] must be_==(true)
      controller.setPlayer(1, new SmartAIPlayer(controller, 1))
      controller.players(1).isInstanceOf[SmartAIPlayer] must be_==(true)
    }
  }
}
