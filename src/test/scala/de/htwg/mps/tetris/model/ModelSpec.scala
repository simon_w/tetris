package de.htwg.mps.tetris.model

import org.specs2.mutable.SpecificationWithJUnit
import de.htwg.mps.tetris.view.swing.styles.StandardTetrisStyle

class ModelSpec extends SpecificationWithJUnit {

  "A new Model" should {
    val model = new SingleGameModel(6, 6, new StandardTetrisStyle)

    "have a falling block" in {
      model.currentBlock must be_!=(null)
    }

    " that can be moved to the left" in {
      model.checkIfCurrentBlockCanBeMoved(-1, 0, 0) must be_==(true)
    }

    " that can be moved to the right" in {
      model.checkIfCurrentBlockCanBeMoved(1, 0, 0) must be_==(true)
    }

    " that can be moved down" in {
      model.checkIfCurrentBlockCanBeMoved(0, 1, 0) must be_==(true)
    }

    " that can be rotated to the left" in {
      model.checkIfCurrentBlockCanBeMoved(0, 0, -1) must be_==(true)
    }

    " that can be moved to the right" in {
      model.checkIfCurrentBlockCanBeMoved(0, 0, 1) must be_==(true)
    }
  }

  "A block touching the left border" should {

    val model = new SingleGameModel(6, 6, new StandardTetrisStyle)
    model.currentBlock.shape = BlockShapes.O
    model.currentBlock.posX = -1

    "not be movable to the left" in {
      model.checkIfCurrentBlockCanBeMoved(-1, 0, 0) must be_==(false)
    }

    "be movable to the right" in {
      model.checkIfCurrentBlockCanBeMoved(1, 0, 0) must be_==(true)
    }
  }

  "A block touching the right border" should {

    val model = new SingleGameModel(6, 6, new StandardTetrisStyle)
    model.currentBlock.shape = BlockShapes.O
    model.currentBlock.posX = 3 // cells in columns 4 & 5 active (the rightmost two)

    "not be movable to the right" in {
      model.checkIfCurrentBlockCanBeMoved(1, 0, 0) must be_==(false)
    }

    "be movable to the left" in {
      model.checkIfCurrentBlockCanBeMoved(-1, 0, 0) must be_==(true)
    }
  }

  "A block touching the bottom border" should {

    val model = new SingleGameModel(6, 6, new StandardTetrisStyle)
    model.currentBlock.shape = BlockShapes.O
    model.currentBlock.posY = 3 // cells in rows 4 & 5 active (last two)

    "not be movable downwards" in {
      model.checkIfCurrentBlockCanBeMoved(0, 1, 0) must be_==(false)
    }
  }

  "A completed row" should {

    // generate a grid 6x6 with the last two rows filled that way 
    // |XXXX  |
    // |XXXX  |
    val model = new SingleGameModel(6, 6, new StandardTetrisStyle)
    model.currentBlock.shape = BlockShapes.O
    for (row <- 4 to 5; column <- 0 to 3) {
      model.grid.squares(row)(column).squareType = 1
    }

    // now move an O-block to the gap an persist it
    model.currentBlock.posX = 3 // cells in columns 4 & 5 active (last two)
    model.currentBlock.posY = 3 // cells in rows 4 & 5 active (last two)

    "be removed" in {
      model.persistBlock
      for (row <- 4 to 5; column <- 0 to 5) {
        model.grid.squares(row)(column).squareType must be_==(0)
      }
    }
  }

  "A model containing a persisted o-shape" should {
    val model = new SingleGameModel(6, 6, new StandardTetrisStyle)
    model.currentBlock.shape = BlockShapes.O
    model.currentBlock.squareType = 2
    model.moveInstantlyDownAndPersistBlock

    "have a grid with a persisted o-shape" in {
      val grid = new Grid(6, 6)
      grid.squares(5)(2) = new Square(2)
      grid.squares(5)(3) = new Square(2)
      grid.squares(4)(2) = new Square(2)
      grid.squares(4)(3) = new Square(2)

      for (row <- 0 to 5; column <- 0 to 5)
        model.grid.squares(row)(column).squareType must be_==(grid.squares(row)(column).squareType)
    }

    "return a grid with a persisted o-shape" in {
      val grid = new Grid(6, 6)
      grid.squares(5)(2) = new Square(2)
      grid.squares(5)(3) = new Square(2)
      grid.squares(4)(2) = new Square(2)
      grid.squares(4)(3) = new Square(2)

      for (row <- 0 to 5; column <- 0 to 5)
        model.getGrid.squares(row)(column).squareType must be_==(grid.squares(row)(column).squareType)
    }
    
    "return its copy containing a grid with a persisted o-shape" in {      
      val copy = model.copy
      for (row <- 0 to 5; column <- 0 to 5)
        model.grid.squares(row)(column).squareType must be_==(copy.grid.squares(row)(column).squareType)
    }
  }

  "A new model containing a persisted o-shape" should {
    val model = new SingleGameModel(10, 20, new StandardTetrisStyle)
    model.currentBlock.shape = BlockShapes.O
    model.currentBlock.squareType = 2
    model.moveInstantlyDownAndPersistBlock

    "return the correct string" in {
      model.toString() must contain("19|    ##    |")
    }
  }
  
  "A game with a filled grid" should {
    "be over" in {
      val model = new SingleGameModel(10, 20, new StandardTetrisStyle)
      model.grid.fill()
      model.checkIfGameIsOver must be_==(true)
    }
  }
  
  "A new model containing singleGameModel with a persisted o-shape" should {
    val model = new Model(3, 10, 20)
    val singleGameModel = new SingleGameModel(10, 20, new StandardTetrisStyle)
    singleGameModel.currentBlock.shape = BlockShapes.O
    singleGameModel.currentBlock.squareType = 2
    singleGameModel.moveInstantlyDownAndPersistBlock
    
    model.models(0)=singleGameModel
    model.models(1)=new SingleGameModel(10, 20, new StandardTetrisStyle)
    model.models(2)=new SingleGameModel(10, 20, new StandardTetrisStyle)

    "return the correct string" in {
      model.toString() must contain("|    ##    |")
    }
    
    "handle multiplayer lines" in {
      model.models(1).completedRowsCount=3
      model.handleMultiplayerLines(1)
      model.toString() must contain("|    ##    |")
    }
    
    "move its currentblock down at timeout" in {
      val yPreMove = model.models(0).currentBlock.posY
      model.timeout
      (model.models(0).currentBlock.posY-yPreMove) must be_==(1)
    }
    
  }

}