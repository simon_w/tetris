package de.htwg.mps.tetris.model
import org.specs2.mutable.SpecificationWithJUnit

class GridSpec extends SpecificationWithJUnit {

  "A new grid" should {
    val grid = new Grid(20, 10)
    
    "have only squares with type 0" in {
      for(y <- 0 until grid.height; x <- 0 until grid.width) {
        grid.squares(y)(x).squareType must be_==(0)
      }
    }
  }
  
  "A new grid of size 5x10" should {
    val grid = new Grid(5, 10)
    
    "be declared 10 squares high and 5 squares width" in {
      grid.height must be_==(10)
      grid.width  must be_==( 5)
    }
    
    "really be 10 squares high and 5 squares width" in {
      var rowNb, columnNb = 0;
      grid.squares.foreach(row => {rowNb += 1; row.foreach(column => {if(rowNb == 1) columnNb += 1})})
      rowNb    must be_==(10)
      columnNb must be_==( 5)
    }
  }
  
}
