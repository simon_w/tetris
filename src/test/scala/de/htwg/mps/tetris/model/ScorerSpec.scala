package de.htwg.mps.tetris.model
import org.specs2.mutable.SpecificationWithJUnit

class ScorerSpec extends SpecificationWithJUnit {

  "A scorer" should{
    val scorer = new Scorer
    
    sequential
    
    "score rows" in {
      scorer.scoreCompletedRows(20)
      for(i <- 0 to 20)
        scorer.scoreBlock
      scorer.level must be_!=(1)
      scorer.rows must be_==(20)
      scorer.score must be_!=(0)
      scorer.shortenIntervalMultiplier must be_<(1.0)
    }
    
    "reset rows" in {
      scorer.resetScore
      scorer.level must be_==(1)
      scorer.rows must be_==(0)
      scorer.score must be_==(0)
      scorer.shortenIntervalMultiplier must be_==(1.0)
    }
    
  }
  
}