package de.htwg.mps.tetris.model
import org.specs2.mutable.SpecificationWithJUnit

class SquareSpec extends SpecificationWithJUnit {

	"A new Square" should {
	  var square = new Square
	  
	  "be of type 0" in {
	    square.squareType must be_==(0)
	  }
	  
	  "print to space" in {
	    square.toString must be_==(" ")
	  }
	}

	"A new Square of type 1" should {
	  var square = new Square(1)
	  
	  "print to '#'" in {
	    square.toString must be_==("#")
	  }
	}
	
	"A new Square of type 2" should {
	  var square = new Square(2)
	  
	  "print to '#'" in {
	    square.toString must be_==("#")
	  }
	}
}